require 'capybara/cucumber'
require 'rspec'
require 'selenium-webdriver'
require 'dotenv'

Dotenv.load
Capybara.default_max_wait_time = 5

Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, browser: ENV["BROWSER"].to_sym, options: Selenium::WebDriver::Chrome::Options.new(args: %w[--headless --no-sandbox --disable-gpu]))
end

Capybara.default_driver = :selenium
